# 创建组件


```
php bin/hyperf.php hei:create crop 100 100
```
# 添加composer
```
    "repositories": {
        "plugin": {
            "type": "path",
            "url": "./plugin/*"
        }
    }
``` 
# 创建好数据库
# 创建代码
```
php bin/hyperf.php hei:gen crop crop -m -s -c -l
```

# 发布配置文件
```
php bin/hyperf.php vendor:publish 96qbhy/hyperf-auth
php bin/hyperf.php vendor:publish haozing/fastcore
```