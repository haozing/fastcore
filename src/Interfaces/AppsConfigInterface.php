<?php

namespace Haozing\FastCore\Interfaces;

interface AppsConfigInterface
{

    /**
     * 获取应用配置
     */
    public function getAppConfig(string $appName, string $baseValue,string $key, array $where = []): array;

    /**
     * 设置应用配置
     */
    public function setAppConfig(string $appName,int $id, string $value): void;
}