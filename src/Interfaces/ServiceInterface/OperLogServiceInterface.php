<?php

namespace Haozing\FastCore\Interfaces\ServiceInterface;

interface OperLogServiceInterface
{
    /**
     * 新增数据
     * @param array $data
     * @return int
     */
    public function save(array $data): int;
}