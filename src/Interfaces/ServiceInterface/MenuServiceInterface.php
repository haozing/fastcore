<?php

namespace Haozing\FastCore\Interfaces\ServiceInterface;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

interface MenuServiceInterface
{
    /**
     * 通过code获取菜单名称
     * @param string $code
     * @return string
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function findNameByCode(string $code): string;
}