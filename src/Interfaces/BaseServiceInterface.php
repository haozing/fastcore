<?php
declare(strict_types=1);
namespace Haozing\FastCore\Interfaces;

interface BaseServiceInterface
{
    /**
     * 查询分类表单条数据 - 根据ID
     * @param int $id ID
     * @param string[] $field 查询字段
     */
    public function getOneById(int $id, array $field = ['*']);

    /**
     * 查询分类表单条数据 - 根据条件
     * @param array $where 条件
     * @param string[] $field 查询字段
     * @param array $options
     */
    public function getOneByWhere(array $where, array $field = ['*'], array $options = []);

    /**
     * 查询分类表多条数据 - 根据ID
     * @param array $ids ID
     * @param string[] $field 查询字段
     */
    public function getManyByIds(array $ids, array $field = ['*']);

    /**
     * 查询分类表多条数据Pluck - 根据ID
     * @param array $ids ID
     * @param string $field 查询字段
     * @return array
     */
    public function getManyPluckByIds(array $ids, string $field = 'id'): array;


    /**
     * 查询分类表多条数据 - 根据条件
     * @param array $where 条件
     * @param string[] $field 查询字段
     * @param array $options
     * @param int $limit
     */
    public function getManyByWhere(array $where, array $field = ['*'], array $options = [], int $limit = 1000);

    /**
     * 查询分类表多条数据Pluck - 根据条件
     * @param array $where 条件
     * @param string $field 查询字段
     * @return array
     */
    public function getManyPluckByWhere(array $where, string $field = 'id'): array;
    /**
     * 查询分类表分页数据 - 根据条件
     * @param array $where 条件
     * @param string[] $field 查询字段
     * @param array $options
     * @return array
     */
    public function getPage(array $where, array $field = ['*'],array $options = []);

    /**
     * 添加分类表单条数据
     * @param array $data 添加数据
     * @return int 自增ID
     */
    public function addOne(array $data): int;

    /**
     * 添加分类表多条数据
     * @param array $data 添加数据
     * @return bool 操作结果
     */
    public function addMany(array $data): bool;

    /**
     * 修改分类表单条数据 - 根据ID
     * @param int $id ID
     * @param array $data 修改数据
     * @return int 修改条数
     */
    public function updateById(int $id, array $data): int;
    /**
     * 修改分类表单条数据 - 根据条件
     * @param array $where 条件
     * @param array $data 修改数据
     * @param array $options
     * @return int 修改条数
     */
    public function updateByWhere(array $where, array $data,array $options=[]): int;
    /**
     * 删除分类表单条数据 - 根据ID
     * @param int $id ID
     * @return int 删除条数
     */
    public function deleteById(int $id): int;

    /**
     * 删除分类表多条数据 - 根据ID
     * @param array $ids ID
     * @return int 删除条数
     */
    public function deleteManyByIds(array $ids): int;
    /**
     * 删除分类表数据 - 根据条件
     * @param array $where 条件
     * @param array $options
     * @return int 删除条数
     */
    public function deleteByWhere(array $where,array $options =[]): int;
    /**
     * 批量修改 - 根据ID
     * @param array $data 修改数据 `[['id'=>1, 'name' => 'name1'], ['id' => 2, 'name' => 'name2']]`
     * @return int 修改条数
     */
    public function updateMany(array $data): int;

    /**
     * 保存关联数据
     * @param string $whereColumn 条件字段
     * @param int $whereId 条件字段的值
     * @param string $DataColumn 保存字段
     * @param array $DataValue   保存的值
     * @return mixed
     */
    public function saveRelationshipById(string $whereColumn,int $whereId,string $DataColumn, array $DataValue): mixed;
    /**
     * 获取list转为map
     * @param array $where 条件
     * @param string $field 查询字段
     * @param string[] $fields 返回的字段
     * @return array
     */
    public function getMap(array $where, string $field, array $fields = ['*']): array;

    /**
     * 根据条件获取某个字段的数量
     */
    public function getCountByWhere(array $where, string $field = 'id', array $options = []): int;

    /**
     * 根据条件获取某个字段的数量,自动注入租户id
     */
    public function getCountByWhereTenant(array $where, string $field = 'id', array $options = []): int;
}