<?php

namespace Haozing\FastCore\Interfaces;

interface AuthUserInterface
{
    //获取菜单权限
    public function getPermissionCodes(array $user): array;

    //获取角色权限
    public function getRoleCodes(array $user): array;

}