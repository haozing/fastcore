<?php

namespace Haozing\FastCore\Service;

use Haozing\FastCore\Interfaces\AuthUserInterface;

class AuthUser implements AuthUserInterface
{

    public function getPermissionCodes(array $user): array
    {
        return $user['permission_codes'] ?? [];
    }

    public function getRoleCodes(array $user): array
    {
        return $user['role_codes'] ?? [];
    }
}