<?php

declare(strict_types = 1);

namespace Haozing\FastCore\Command;

use Hyperf\Command\Annotation\Command;
use Hyperf\Database\Seeders\Seed;
use Hyperf\Database\Migrations\Migrator;
use Haozing\FastCore\Core;
use Throwable;
use function Hyperf\Support\make;
/**
 * Class UpdateProjectCommand
 * @package System\Command
 */
#[Command]
class UpdateProjectCommand extends CoreCommand
{
    /**
     * 更新项目命令
     * @var string|null
     */
    protected ?string $name = 'hei:update';

    protected array $database = [];

    protected Seed $seed;

    protected Migrator $migrator;

    /**
     * UpdateProjectCommand constructor.
     * @param Migrator $migrator
     * @param Seed $seed
     */
    public function __construct(Migrator $migrator, Seed $seed)
    {
        parent::__construct();
        $this->migrator = $migrator;
        $this->seed = $seed;
    }

    public function configure(): void
    {
        parent::configure();
        $this->setHelp('run "php bin/hyperf.php hei:update" Update heiAdmin system');
        $this->setDescription('heiAdmin system update command');
    }

    /**
     * @throws Throwable
     */
    public function handle(): void
    {
        $modules = make(Core::class)->getModuleInfo();
        $this->migrator->setConnection('default');

        foreach ($modules as $name => $module) {
            if (!isset($module['pluginPath'])) {
                continue;
            }
            $seedPath = $module['pluginPath']. '/Database/Seeders/Update';
            $migratePath = $module['pluginPath'] . '/Database/Migrations/Update';

            if (is_dir($migratePath)) {
                $this->migrator->run([$migratePath]);
            }

            if (is_dir($seedPath)) {
                $this->seed->run([$seedPath]);
            }
        }

        redis()->flushDB();

        $this->line($this->getGreenText('updated successfully...'));
    }
}
