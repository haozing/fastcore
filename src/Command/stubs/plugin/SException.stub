<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace %NAME%\Exception;

use %NAME%\ErrorCode\SError;
use Haozing\FastCore\ExceptionCode\Exception\AbstractErrorException;
use Throwable;

class SException extends AbstractErrorException
{
    public function __construct(string|int $code = 0, string $message = null, Throwable $previous = null)
    {
        //如果$code不是数字
        if (!is_numeric($code)) {
            $code = make(SError::class)->getTargetCode($code);
        }

        if (is_null($message)) {
            $message = SError::getMessage($code);
        }

        $this->data['httpCode'] = SError::getHttpCode($code);

        parent::__construct($message, $code, $previous);
    }
}
