<?php
declare(strict_types = 1);
namespace Haozing\FastCore\Command;

use Hyperf\Command\Annotation\Command;
use Hyperf\Stringable\Str;
use Symfony\Component\Console\Input\InputOption;
use Throwable;

/**
 * Class JwtCommand
 * @package System\Command
 */
#[Command]
class JwtCommand extends CoreCommand
{
    /**
     * 生成JWT密钥命令
     * @var string|null
     */
    protected ?string $name = 'hei:jwt-gen';

    public function configure(): void
    {
        parent::configure();
        $this->setHelp('run "php bin/hyperf.php hei:gen-jwt" create the new jwt secret');
        $this->setDescription('heiAdmin system gen jwt command');
    }

    /**
     * @throws Throwable
     */
    public function handle(): void
    {
        $jwtSecret = Str::upper($this->input->getOption('jwtSecret'));

        if (empty($jwtSecret)) {
            $this->line('Missing parameter <--jwtSecret < jwt secret name>>', 'error');
        }

        $envPath = BASE_PATH . '/.env';

        if (! file_exists($envPath)) {
            $this->line('.env file not is exists!', 'error');
        }

        $key = base64_encode(random_bytes(64));

        if (Str::contains(file_get_contents($envPath), $jwtSecret) === false) {
            file_put_contents($envPath, "\n{$jwtSecret}={$key}\n", FILE_APPEND);
        } else {
            file_put_contents($envPath, preg_replace(
                "~{$jwtSecret}\s*=\s*[^\n]*~",
                "{$jwtSecret}=\"{$key}\"",
                file_get_contents($envPath)
            ));
        }

        $this->info('jwt secret generator successfully:' . $key);

    }

    protected function getOptions(): array
    {
        return [
            ['jwtSecret', '', InputOption::VALUE_REQUIRED, 'Please enter the jwtSecret to be generated'],
        ];
    }


}