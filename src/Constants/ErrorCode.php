<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Haozing\FastCore\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * 500: '服务器发生错误', //常用
 * 200: '服务器成功返回请求数据', //常用
 * 201: '新建或修改数据成功',
 * 202: '一个请求已经进入后台排队(异步任务)',
 * 204: '删除数据成功',
 * 400: '发出信息有误',
 * 401: '用户没有权限(令牌失效、用户名、密码错误、登录过期)', //常用
 * 402: '令牌过期', //常用
 * 403: '用户得到授权，但是访问是被禁止的',
 * 404: '访问资源不存在',
 * 406: '请求格式不可得',
 * 410: '请求资源被永久删除，且不会被看到',
 * 422: '非法的参数',
 * 502: '网关错误',
 * 503: '服务不可用，服务器暂时过载或维护',
 * 504: '网关超时',
 * @method static string getMessage(int $code)  获取错误码信息
 * @method static int getHttpCode(int $code) 获取错误码的httpCode
 */
#[Constants]
class ErrorCode extends AbstractConstants
{
    /**
     * 服务器发生错误
     * @Message("服务器发生错误")
     * @HttpCode("200")
     */
    const SERVER_ERROR = 500;

    /**
     * 服务器成功返回请求数据
     * @Message("服务器成功返回请求数据")
     * @HttpCode("200")
     */
    const SUCCESS = 200;

    /**
     * 新建或修改数据成功
     * @Message("新建或修改数据成功")
     * @HttpCode("200")
     */
    const CREATED = 201;

    /**
     * 一个请求已经进入后台排队(异步任务)
     * @Message("一个请求已经进入后台排队(异步任务)")
     * @HttpCode("200")
     */
    const ACCEPTED = 202;

    /**
     * 删除数据成功
     * @Message("删除数据成功")
     * @HttpCode("200")
     */
    const NO_CONTENT = 204;

    /**
     * 发出信息有误
     * @Message("发出信息有误")
     * @HttpCode("200")
     */
    const BAD_REQUEST = 400;

    /**
     * 用户没有权限(令牌失效、用户名、密码错误、登录过期)
     * @Message("用户没有权限(令牌失效、用户名、密码错误、登录过期)")
     * @HttpCode("200")
     */
    const UNAUTHORIZED = 401;

    /**
     * 令牌过期
     * @Message("令牌过期")
     * @HttpCode("200")
     */
    const TOKEN_EXPIRED = 402;

    /**
     * 用户得到授权，但是访问是被禁止的
     * @Message("用户得到授权，但是访问是被禁止的")
     * @HttpCode("200")
     */
    const FORBIDDEN = 403;

    /**
     * 访问资源不存在
     * @Message("访问资源不存在")
     * @HttpCode("200")
     */
    const NOT_FOUND = 404;

    /**
     * 请求格式不可得
     * @Message("请求格式不可得")
     * @HttpCode("200")
     */
    const NOT_ACCEPTABLE = 406;

    /**
     * 请求资源被永久删除，且不会被看到
     * @Message("请求资源被永久删除，且不会被看到")
     * @HttpCode("200")
     */
    const GONE = 410;

    /**
     * 非法的参数
     * @Message("非法的参数")
     * @HttpCode("200")
     */
    const INVALID_PARAM = 422;
    static string $INVALID_PARAM = "422:非法的参数";

    /**
     * 服务器异常(third-party-api)
     * @Message("服务器异常(third-party-api)")
     * @HttpCode("500")
     */
    const THIRD_API_ERROR = 100015;

    /**
     * 网关错误
     * @Message("网关错误")
     * @HttpCode("200")
     */
    const BAD_GATEWAY = 502;

    /**
     * 服务不可用，服务器暂时过载或维护
     * @Message("服务不可用，服务器暂时过载或维护")
     * @HttpCode("200")
     */
    const SERVICE_UNAVAILABLE = 503;

    /**
     * 网关超时
     * @Message("网关超时")
     * @HttpCode("200")
     */
    const GATEWAY_TIMEOUT = 504;


}
