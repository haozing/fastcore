<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Haozing\FastCore;

use Haozing\FastCore\Annotation\DependProxyCollector;
use Haozing\FastCore\Exception\Handler\AuthExceptionHandler;
use Haozing\FastCore\Exception\Handler\CommonExceptionHandler;
use Haozing\FastCore\Exception\Handler\ErrorExceptionHandler;
use Haozing\FastCore\Exception\Handler\GuzzleRequestExceptionHandler;
use Haozing\FastCore\Exception\Handler\ValidationExceptionHandler;
use Haozing\FastCore\ExceptionCode\ErrorCollector;
use Haozing\FastCore\Interfaces\AuthUserInterface;
use Haozing\FastCore\Middleware\CorsMiddleware;
use Haozing\FastCore\Middleware\TenantMiddleware;
use Haozing\FastCore\Service\AuthUser;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                AuthUserInterface::class => AuthUser::class,
            ],
            'exceptions' => [
                'handler' => [
                    'http' => [
                        AuthExceptionHandler::class,
                        //GuzzleRequestExceptionHandler::class,
                        ValidationExceptionHandler::class,
                        CommonExceptionHandler::class,
                    ],
                ],
            ],
            'middlewares' => [
                'http' => [
                    CorsMiddleware::class,
                    TenantMiddleware::class
                ],
            ],
            'commands' => [
            ],
            'listeners' => [
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                    'collectors' => [
                        DependProxyCollector::class,
                    ]
                ],
            ],
            'publish' => [
                [
                    'id' => 'config',
                    'description' => 'fastCore config file.', // 描述
                    // 建议默认配置放在 publish 文件夹中，文件命名和组件名称相同
                    'source' => __DIR__ . '/../publish/fastCore.php',  // 对应的配置文件路径
                    'destination' => BASE_PATH . '/config/autoload/fastCore.php', // 复制为这个路径下的该文件
                ],
            ],
            'plugin' => [
                'order' => 99,//权重
                'errorCode' => '100',//错误码
                'hooks' => [],//钩子
                'events' => [],//事件
            ]
        ];
    }
}
