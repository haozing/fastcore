<?php

namespace Haozing\FastCore\Helper;

use Hyperf\Context\ApplicationContext;
use Hyperf\Logger\LoggerFactory;

class Log
{
    public static function get(string $name = 'sys', string $group = 'default')
    {
        return ApplicationContext::getContainer()->get(LoggerFactory::class)->get($name,$group);
    }
}