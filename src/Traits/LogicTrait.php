<?php
declare(strict_types = 1);
namespace Haozing\FastCore\Traits;

use Haozing\FastCore\Abstract\AbstractService;

trait LogicTrait
{
    public AbstractService $service;
}