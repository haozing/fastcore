<?php

namespace Haozing\FastCore\Controller;

use Haozing\FastCore\Abstract\AbstractController;
use Haozing\FastCore\Constants\ErrorCode;
use Haozing\FastCore\Exception\CommonException;
use Haozing\FastCore\Utils\OpenDoc\ApiService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

#[Controller]
class ApiDocController extends AbstractController
{

    #[Inject]
    private ApiService $apiService;
    /**
     * 获取接口明细
     * @return array
     */
    public function getApiDetail()
    {
        //
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[RequestMapping(path: "/open-doc/get-apps", methods: "get,post")]
    public function getApps(): \Psr\Http\Message\ResponseInterface
    {
        $data = $this->apiService->getAllApiMenus();
        return $this->success($data);
    }

    /**
     * 获取内部的所有二级菜单
     */
    #[RequestMapping(path: "/open-doc/get-menu", methods: "get,post")]
    public function getMenu(): \Psr\Http\Message\ResponseInterface
    {
        $params = $this->request->all();
        $validator = $this->validationFactory->make($params,
            [
                'app_name' => 'required',
                'class_name' => 'required',
                'method_name' => 'required',
            ],
            [
                'app_name.required' => '应用标识不能为空',
                'class_name.required' => '应用名称不能为空',
                'method_name.required' => '版本不能为空',
            ]
        );

        if ($validator->fails()){
            throw new CommonException(ErrorCode::INVALID_PARAM,$validator->errors()->first());
        }
        $data = $this->apiService->getMenu($params['app_name'],$params['class_name'],$params['method_name']);
        return $this->success($data);
    }

    /**
     * 获取所有的接口
     */
    public function getApis()
    {
        // 获取所有的接口
    }
}