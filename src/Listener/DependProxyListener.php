<?php

declare(strict_types=1);

/**
 * MineAdmin is committed to providing solutions for quickly building web applications
 * Please view the LICENSE file that was distributed with this source code,
 * For the full copyright and license information.
 * Thank you very much for using MineAdmin.
 *
 * @Author @小小只^v^ <littlezov@qq.com>, X.Mo<root@imoi.cn>
 * @Link   https://gitee.com/xmo/MineAdmin
 */

namespace Haozing\FastCore\Listener;

use Haozing\FastCore\Annotation\DependProxyCollector;
use Haozing\FastCore\Factory\DependProxyFactory;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BootApplication;

#[Listener]
class DependProxyListener implements ListenerInterface
{
    public function listen(): array
    {
        return [ BootApplication::class ];
    }

    public function process(object $event): void
    {
        foreach (DependProxyCollector::getDependencies() as $target => $definition) {
            DependProxyFactory::define($target, $definition);
        }
    }
}
