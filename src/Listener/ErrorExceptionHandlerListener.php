<?php

namespace Haozing\FastCore\Listener;

use ErrorException;
use Haozing\FastCore\Helper\Log;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BootApplication;

class ErrorExceptionHandlerListener implements ListenerInterface
{
    public function listen(): array
    {
        return [
            BootApplication::class,
        ];
    }

    public function process(object $event): void
    {
        set_error_handler(static function ($level, $message, $file = '', $line = 0): bool {
            if (error_reporting() & $level) {
                Log::get()->error(sprintf('%s in %s on line %d', $message, $file, $line));
                throw new ErrorException($message, 0, $level, $file, $line);
            }

            return true;
        });
    }
}