<?php

namespace Haozing\FastCore\Model;

use Haozing\FastCore\Context\UserContext;
use Hyperf\Context\Context;
use Hyperf\Database\Model\Builder;

use function Hyperf\Config\config;
trait TenantModelTrait
{

    /**
     * 注册查询注入租户ID的方法
     * @return void
     */
    private function registerTenant(): void
    {
        $model = $this;
        // 数据权限方法
        Builder::macro('useTenant', function () use ($model) {
            //需要有租户信息
            $tenantId = Context::get("pp-tenant-code");
            if (!empty($tenantId) && $tenantId != -1) {
                if (in_array('tenant_id', $model->getField())){
                    return $this->where('tenant_id', $tenantId);
                }
            }
            return $this;
        });
    }
}