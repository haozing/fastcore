<?php

namespace Haozing\FastCore\Model\Interfaces;

use Hyperf\Database\Model\Builder;

interface ConditionInterface
{
    /**
     * 执行附加条件
     *
     * @param Builder $builder 查询构造器
     * @return Builder
     */
    public function apply(Builder $builder): Builder;

}