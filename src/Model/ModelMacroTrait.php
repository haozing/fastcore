<?php

namespace Haozing\FastCore\Model;

use Haozing\FastCore\Model\Interfaces\ConditionInterface;
use Hyperf\Database\Model\Builder;

trait ModelMacroTrait
{
    private function registerCondition(ConditionInterface $condition)
    {
        // 执行方法
        Builder::macro('conditionWhere', function () use ($condition) {
            /* @var Builder $this */
            return $condition->apply($this);
        });
    }
}