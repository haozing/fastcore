<?php

namespace Haozing\FastCore\Utils\Auth;

class DataScopeType
{
    // 全部数据权限
    const ALL_SCOPE = 1;

    //部门数据权限，根据创建人
    const DEPT_SCOPE = 2;

    //本人数据权限
    const SELF_SCOPE = 3;

    //部门数据权限，根据部门id
    const CUSTOM_SCOPE_ID = 4;

    //部门数据权限，根据更新人
    const CUSTOM_SCOPE_UPDATE = 5;

    //部门数据权限，排除更新人
    const CUSTOM_SCOPE_UPDATE_EXCEPT = 6;

    //部门数据权限，排除创建人
    const CUSTOM_SCOPE_CREATE_EXCEPT = 7;

    //部门数据权限，根据时间和排序
    const CUSTOM_SCOPE_TIME = 8;

    //部门数据权限，根据数量和排序
    const CUSTOM_SCOPE_COUNT = 9;

    //部门数据权限，根据表字段
    const CUSTOM_SCOPE_FIELD = 10;

    //部门数据权限，根据表条件
    const CUSTOM_SCOPE_FIELD_CONDITION = 11;
}