<?php

namespace Haozing\FastCore\Annotation\OpenDoc;

use Attribute;

/**
 * 请求头
 * @Target({"METHOD"})
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class ApiHeader extends AbstractAnnotation
{

    /**
     * @param string $name 字段名
     * @param string $type 字段类型
     * @param string $desc 字段名称
     * @param bool $require 是否必须
     * @param string $mock Mock规则
     */
    public function __construct(
        public string $name = '',
        public string $type = '',
        public bool   $require = false,
        public string $desc = '',
        public string $mock = ""
    )
    {}

}
