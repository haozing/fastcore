<?php

namespace Haozing\FastCore\Annotation\OpenDoc;

use Attribute;

/**
 * 请求参数
 * @Target({"METHOD","ANNOTATION"})
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class ApiParam extends AbstractAnnotation
{

    /**
     * @param string $name 字段名
     * @param string $type 字段类型
     * @param bool $require 是否必须
     * @param string|int|bool $default 默认值
     * @param string $desc 字段名称
     * @param string $mock Mock规则
     * @param array $children 子参数
     * @param string $childrenField 为tree类型时指定children字段
     * @param string $childrenDesc 为tree类型时指定children字段说明
     * @param string $childrenType 为array类型时指定子节点类型
     */
    public function __construct(
        public string          $name = '',
        public string          $type = '',
        public bool            $require = false,
        public bool|int|string $default = "",
        public string          $desc = '',
        public string          $mock = "",
        public array           $children = [],
        public string          $childrenField = "",
        public string          $childrenDesc = "children",
        public string          $childrenType = "",
    )
    {}


}
