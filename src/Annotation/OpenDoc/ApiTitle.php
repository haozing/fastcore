<?php

namespace Haozing\FastCore\Annotation\OpenDoc;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class ApiTitle extends AbstractAnnotation
{
    /**
     * @param string|null $value
     */
    public function __construct(public ?string $value = null){}
}