<?php

namespace Haozing\FastCore\Annotation\OpenDoc;

use Attribute;

/**
 * 描述
 * @Target({"METHOD","CLASS"})
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class EventDesc extends AbstractAnnotation
{
    /**
     * @param string $value 描述
     */
    public function __construct(public string $value)
    {}
}
