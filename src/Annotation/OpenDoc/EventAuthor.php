<?php

namespace Haozing\FastCore\Annotation\OpenDoc;

use Attribute;

/**
 * 作者
 * @Target({"METHOD"})
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class EventAuthor extends AbstractAnnotation
{
    /**
     * @param string $value 作者名称
     */
    public function __construct(public string $value)
    {}
}
