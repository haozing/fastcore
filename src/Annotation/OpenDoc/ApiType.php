<?php

namespace Haozing\FastCore\Annotation\OpenDoc;

use Attribute;

/**
 * 参数类型
 * @Target({"METHOD"})
 */
#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
class ApiType extends AbstractAnnotation
{
    /**
     * @param string $value 参数类型，formData
     */
    public function __construct(public string $value)
    {}
}
