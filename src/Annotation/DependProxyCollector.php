<?php
declare(strict_types = 1);

namespace Haozing\FastCore\Annotation;

use Hyperf\Di\MetadataCollector;

/**
 * 依赖代理收集器
 */
class DependProxyCollector extends MetadataCollector
{
    protected static array $container = [];



    public static function setAround(string $class, $value): void
    {
        static::$container[$class] = $value;
    }

    public static function getDependencies(): array
    {
        if (empty(self::$container)) {
            return [];
        }
        $dependencies = [];
        foreach (self::$container as $collector) {
            $targets = $collector->values;
            $definition = $collector->provider;
            foreach ($targets as $target) {

                //查找$dependencies[$target]是否有值，如果有值，则查询权重
                if (isset($dependencies[$target])) {
                    $priority = self::$container[$dependencies[$target]]->priority;
                    if (isset($priority) && $priority > $collector->priority) {
                        continue;
                    }
                }
                $dependencies[$target] = $definition;
            }
        }
        return $dependencies;
    }
}
