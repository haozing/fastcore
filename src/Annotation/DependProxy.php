<?php

declare(strict_types = 1);

namespace Haozing\FastCore\Annotation;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;
use function Hyperf\Support\make;

/**
 * 依赖代理注解，用于平替某个类
 */
#[Attribute(Attribute::TARGET_CLASS)]
class DependProxy extends AbstractAnnotation
{
    /**
     * @param string[] $values 接口列表
     * @param string|null $provider 代理类
     * @param int $priority 代理优先级，越大越优先
     */
    public function __construct(public array $values = [], public ?string $provider = null,public int $priority = 1){}

    public function collectClass(string $className): void
    {
        if (! $this->provider) {
            $this->provider = $className;
        }
        if (count($this->values) == 0 && class_exists($className)) {
            $reflectionClass = new \ReflectionClass(make($className));
            $this->values = array_keys($reflectionClass->getInterfaces());
        }
        //parent::collectClass($className);
        DependProxyCollector::setAround($className, $this);
    }
}
