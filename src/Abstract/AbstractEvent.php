<?php

namespace Haozing\FastCore\Abstract;

abstract class AbstractEvent
{
    // 事件输入的参数
    public array $params;

    // 事件返回的参数
    public array $returns;

    /**
     * @param $params array 事件输入的参数
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * 获取事件输入的参数
     * @return array 事件输入的参数
     */
    public function getParams(): array
    {
        return $this->params;
    }


    /**
     * 设置事件返回的参数
     * @param $returns array 事件返回的参数
     * @return void
     */
    public function setReturns(array $returns): void
    {
        $this->returns = $returns;
    }

    /**
     * 获取事件返回的参数
     * @return array 事件返回的参数
     */
    public function getReturns(): array
    {
        return $this->returns;
    }
}