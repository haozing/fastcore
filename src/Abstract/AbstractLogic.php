<?php
declare(strict_types = 1);
namespace Haozing\FastCore\Abstract;

use Haozing\FastCore\Helper\Tenant;
use Hyperf\Context\Context;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;

abstract class AbstractLogic
{

    protected ValidatorFactoryInterface $validationFactory;

    protected LoggerFactory $logger;

    protected ConfigInterface $config;

    public function __construct(ValidatorFactoryInterface $validationFactory, LoggerFactory $logger, ConfigInterface $config)
    {
        $this->validationFactory = $validationFactory;
        $this->logger = $logger;
        $this->config = $config;
    }
    /**
     * 把数据设置为类属性
     * @param array $data
     */
    protected function setAttributes(array $data): void
    {
        Context::set('attributes', $data);
    }

    /**
     * 魔术方法，从类属性里获取数据
     * @param string $name
     * @return mixed|string
     */
    public function __get(string $name)
    {
        return $this->getAttributes()[$name] ?? '';
    }

    /**
     * 获取数据
     * @return array
     */
    public function getAttributes(): array
    {
        return Context::get('attributes', []);
    }

    /**
     * 租户数据处理
     * @param array $data
     * @return array
     */
    protected function tenantDataHandle(array $data): array
    {
        //先判断有没有开启租户
        if (!$this->config->get('tenant.enable')) {
            return $data;
        }
        return Tenant::dataHandle($data);
    }
}