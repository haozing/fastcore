<?php

namespace Haozing\FastCore\Exception;


use Hyperf\Server\Exception\RuntimeException;

class NoPermissionException extends RuntimeException
{

}