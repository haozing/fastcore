<?php
declare(strict_types=1);
namespace Haozing\FastCore\Exception\Handler;

use Haozing\FastCore\Log\RequestIdHolder;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Haozing\FastCore\Constants\ErrorCode;
use Hyperf\Validation\ValidationException;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class ValidationExceptionHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response): MessageInterface|ResponseInterface
    {
        $this->stopPropagation();

        /** @var ValidationException $throwable */
        $falseMsg = $throwable->validator->errors()->first();

        ## 格式化输出
        $data = responseDataFormat(ErrorCode::INVALID_PARAM, $falseMsg);
        $data['requestId'] = RequestIdHolder::getId();
        $dataStream = new SwooleStream(json_encode($data, JSON_UNESCAPED_UNICODE));

        return $response->withAddedHeader('Content-Type', 'application/json;charset=utf-8')
            ->withStatus($throwable->status)
            ->withBody($dataStream);
    }

    public function isValid(Throwable $throwable): bool
    {
        $validateException = ValidationException::class;
        if (class_exists($validateException) && $throwable instanceof $validateException) {
            return true;
        }
        return false;
    }
}
